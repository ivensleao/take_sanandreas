const Queue  = require('./models/Queue.js');

function Graph(){
    //Setting INFINITY value in JavaScript
    var INFINITY = 1 / 0;
    this.vertices = {};
    this.routes = {};
    this.orders = [];
    
    this.addRoutes = function (routes) {
      this.routes = routes;
    };
    this.pushOrders = function (order) {
      this.orders.push(order);
    };
    this.init = function () {
      var generatedRoutes = [];
      if (this.routes) {
        var groupedRoutes = this.routes.entries();
        
        for (var obj of groupedRoutes) {
          var cityName = obj[0];
          var edgesList = obj[1];          
          var edges = {};
          
          for(var i=0;i<(edgesList.length);i++){
            var auxObj = {[Object.keys(edgesList[i])] : Object.values(edgesList[i]) };
            Object.assign(edges, auxObj);
          }
          this.addVertex(cityName, edges);          
        }
      }
      if (this.orders.length > 0) {
        for (const order of this.orders) {
          generatedRoutes.push(this.shortestPath(order.from, order.to));
        }
      }
      return generatedRoutes;
    };
    this.addVertex = function (name, edges) {
      this.vertices[name] = edges;
    };
    this.shortestPath = function (start, finish) {
      var nodes = new Queue(), distances = {}, previous = {}, path = [], smallest, vertex, neighbor, alt, totalDistance = 0;
      
      for (vertex in this.vertices) {
        if (vertex === start) {
          distances[vertex] = 0;
          nodes.enqueue(0, vertex);
        }
        else {
          distances[vertex] = INFINITY;
          nodes.enqueue(INFINITY, vertex);
        }
        previous[vertex] = null;
      }
      while (!nodes.isEmpty()) {
        smallest = nodes.dequeue();
        
        if (smallest === finish) {
          path = [];
          while (previous[smallest]) {
            path.push(smallest);
            smallest = previous[smallest];
          }
          break;
        }
        if (!smallest || distances[smallest] === INFINITY) {
          continue;
        }
        for (neighbor in this.vertices[smallest]) {
          alt = distances[smallest] + this.vertices[smallest][neighbor];
          if (alt < distances[neighbor]) {
            distances[neighbor] = alt;
            previous[neighbor] = smallest;
            nodes.enqueue(alt, neighbor);
          }
          
        }       
        
      }
     
      if(typeof(distances[vertex]) === 'string'){
        var nDistance = distances[vertex].split('');
        for(var i = 0; i < nDistance.length; i++){
          totalDistance += parseInt(nDistance[i]);
        }
      }else if(typeof(distances[vertex]) === "number"){
        totalDistance = 1;
      }
      return path.concat([start]).reverse() + " " + totalDistance;
    };
  
}

module.exports = Graph;