class Route {
  constructor(cityName, neighbors) {
    this.cityName = cityName;
    this.neighbors = neighbors;    
  }
}
module.exports.Route = Route;