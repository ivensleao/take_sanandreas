# Take - Problema: Correios de San Andreas

This repository is dedicated to solving the technical activity of Take's selection process for the position of Technical Product Manager.

The activity statement can be found in the file 'Take-Correios-de-San-Andreas_v2.pdf' in the project's root directory.

The proposed technology for the solution of the activity was NodeJS / JavaScript. The tests were performed using [Mocha](https://mochajs.org/).

## Solution presented

The proposed activity describes a classic computational / algorithmic problem: The Shortest Path Problem.

Assuming each city/county as a vertex and the time (distance) to travel from one city to another, as an edge, and not necessarily having a way back between the cities, I considered using a classic and well-consolidated algorithm, [the Algorithm of Dijkstra (or Dijkstra Algorithm of shortest path)](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm).

The choice for this algorithm to the detriment of others of the same style (ex .: Bellman-Ford algorithm, etc.) or graph search algorithms, was given by the description of the problem, the edges being equal to or greater than zero and search between all edges from a vertex to find the shortest path.

For the other points that make up the activity solution, the following were used:

* Objects and functions in JavaScript;
* Reading and writing text files (txt format);
* Tests using Mocha;

### Getting Started

[Install NodeJS] - (https://nodejs.org/en/download/)

[Install npm] - (https://www.npmjs.com/get-npm)

### Installing

After installing and configuring the session above:
* Install project packages and dependencies running: $ npm install

### Project Structure

```
├── data
├── models 
├── node-modules
├── results
├── test
```
* data: Original txt files to be used as basis of the entire activity. 
* models: Object models.
* node-modules: Regular node modules.
* results: Output result file will be generated inside this folder in txt format.
* test: Mocha test files.

Obs.: The other files present in the project, such as 'app.js', READ.ME, among others, are in the root directory.

### Running the project

* To run the project, inside the project root directory, run: $ node app.js

### Running the tests

* To run the tests, inside the project root directory, run: $ npm run test

### Authors

* **Ivens Leão** - *Initial work* - [@ivensleao](https://www.linkedin.com/in/ivensleao/)

### License

This project is licensed under the MIT License.
