const fs = require('fs');
const Graph = require('./graph.js')

const { Route } = require("./models/Route");
const { Order } = require("./models/Order");

//Controller and orchestrator of all flow
var graph = new Graph();
var resultFilePath = "./results/rotas.txt";

readRoutes();
readOrders();

function writeToFile(path, data){
  if(path){
    fs.appendFile(path, data , function (err) {
      if (err) throw err;
    });
 }
}

//Order and group an array acording to a property
function groupArrayByProperty(arr, prop, groupProp) {
  const map = new Map(Array.from(arr, obj => [obj[prop], []]));
  arr.forEach(
    obj => map.get(obj[prop]).push(obj[groupProp])
  );
  
  return map;  
}


function readRoutes(){
  var routes = [];
  var routesMap = {};
  //Open and read file 'trechos.txt'
  fs.readFile("./data/trechos.txt", function(error, data) {
    if (error) { throw error; }

    data.toString().split("\n").forEach(function(line, index, arr) {
      if (index === arr.length - 1 && line === "") { return; }
      var lineValues = line.split(' ');

      //Considering the file structure: {City} {City} {Distance}
      if(lineValues && lineValues.length == 3){
        var route = new Route(lineValues[0], {[lineValues[1]]: lineValues[2]});
        routes.push(route);
      }      
    });

    routesMap = groupArrayByProperty(routes,  "cityName", "neighbors");
    graph.addRoutes(routesMap);
    
  });
}

function readOrders() {
  var generatedRoutes = {};
  var fileData = "";

  //Open and read file 'encomendas.txt'
  fs.readFile("./data/encomendas.txt", function(error, data) {
    if (error) { throw error; }

    data.toString().split("\n").forEach(function(line, index, arr) {
      if (index === arr.length - 1 && line === "") { return; }
      var lineValues = line.split(' ');

      //Considering the file structure: {City} {City}
      if(lineValues && lineValues.length == 2){
        var order = new Order(lineValues[0], lineValues[1]);
        graph.pushOrders(order);
      }
    });
    
    generatedRoutes = graph.init(); 
    if(generatedRoutes && generatedRoutes.length > 0){
      for(var i = 0; i < generatedRoutes.length; i++){
        fileData += generatedRoutes[i] + "\n";
      }
    }
    writeToFile(resultFilePath, fileData);
  });
}